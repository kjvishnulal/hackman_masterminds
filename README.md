# README #

This repository can be used by team MASTER MINDS to upload their source code

### What is this repository for? ###

This repository holds the files and folders of source code

### How do I get set up? ###

Clone the "develop" stream of the repository to your local.
Refer[https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html]
Add and push the codes to develop stream.
Create a pull request to "master" stream.


### Who do I talk to? ###

Vishnulal K J or Any volunteers present in the venue


## How to Run the application ##
 
 This is a very high level prototype and dont have any data base configuration.
 
 To run the app open the login.html in chrome and switch to Mobile/RWD Tester,this is a plugin for testing the mobile application.
 
 As this is a mobile app POC switch to this plugin so that the design will looks good.
 
 Give any sample username and password to login.
